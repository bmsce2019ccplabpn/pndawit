#include <stdio.h>

int main()
{   int n;
    printf("HOW MANY ELEMENTS?");
    scanf("%d", &n);
    int i, large, small, temp, smallpos, largepos, arr[n];
        for(i=0;i<n;i++)
        {   
           printf("enter element %d:", i);
           scanf("%d", &arr[i]);
	    }
        printf("Before exchange:\n");
        for(i=0;i<n;i++)
        {   
           printf("  %d", arr[i]);
	    }
          
          small= arr[0];
          large= arr[0];
          smallpos=0;
          largepos=0;
         for(i=1;i<n;i++)
        {   
           if(arr[i]<small)
            {  small=arr[i];
              smallpos=i;  }
           if(arr[i]>large)
         {   large=arr[i];
               largepos=i;}
        }
        printf("\nsmallest is %d in position [%d]", small, smallpos);
        printf("\nLARGEST is %d  in position [%d]", large, largepos);
          
          temp=arr[smallpos];
          smallpos=arr[largepos];
          arr[largepos]=temp;
          printf("\nAfter exchange: \n");
          for(i=0;i<n;i++)
        {   
           printf("  %d", arr[i]);
	    }
        printf("\n");
}