#include <stdio.h>
#include <stdlib.h>

int main()
{
   int R, C, r, c,i,j;
   printf("Enter the number of row and column of matrix 1 \n");
   scanf("%d %d", &R, &C);
   printf("Enter the number of row and column  of matrix 2 \n");
   scanf("%d %d", &r, &c);
   if(r!=R||c!=C)
  { printf("Unable to execute addition \n");
    exit(0);
    }
    int A[R][C], B[r][c];
    printf("Enter the first matrix \n");
     for(i=0;i<R;i++)
     {
         for(j=0;j<C;j++)
            scanf("%d", &A[i][j]);
     }
    printf("Enter the second matrix \n");
     for(i=0;i<R;i++)
     {
         for(j=0;j<C;j++)
            scanf("%d", &B[i][j]);
     }
     printf("The addition of these is: \n");
     for(i=0;i<R;i++)
     {
         for(j=0;j<C;j++)
          printf("%d ", A[i][j]+B[i][j]);
          printf("\n");
     }
    return 0;
}
