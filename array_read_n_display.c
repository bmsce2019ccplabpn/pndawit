#include <stdio.h>
#include <stdlib.h>

int main()
{   int n;
    printf("How many elements?\n");
    scanf("%d", &n);
     int i, arr[n];
        for(i=0;i<n;i++)
        {
            printf("Enter element %d:", i);
            scanf("%d", &arr[i]);
        }
        printf("Elements are: \n");
        for(i=0;i<n;i++)
        {
            printf("%d  ", arr[i]);
        }

    return 0;
}
