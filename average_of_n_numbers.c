#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, n, m, sum=0;
    printf("Enter the number of elements");
    scanf("%d", &n);
    for(i=1;i<=n;i++)
    {   printf("Enter element %d: ", i);
        scanf("%d", &m);
        sum+=m;
    }
    printf("The average of the numbers is %.2f", (float)sum/n);
    return 0;
}
