#include <stdio.h>

int main()
{  int i, sum,n;
   
     printf("Enter n\n");
     scanf("%d", &n);
     sum=0;
     i=1;
      while(i<=n)
          {sum+=i;
          i++;}
    
     printf("The average of first 'n' natural numbers is  %.2f \n", (float)sum/n);  
	return 0;
}
