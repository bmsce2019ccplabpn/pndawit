#include <stdio.h>
int main()
{
   FILE *dd;
   char c;
   printf("Enter data:\n");
   dd=fopen("input.dat","w");
   while((c=getchar())!= EOF)
         fputc(c,dd);

   fclose(dd);
   printf("The entered data is: \n");
   dd=fopen("input.dat","r");
   while((c=fgetc(dd))!= EOF)
         putchar(c);
   fclose(dd);
}
