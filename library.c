#include <stdio.h>
#include <stdlib.h>

int main()
{
    struct dop
    {
        int dd;
        int mm;
        int yy;
    };

    struct stud
    {
        char name[20];
        char author[20];
        int code;
        struct dop date;
    }b;
    printf("Enter name, author and ISBN code: \n");
    scanf("%s %s %d",b.name, b.author, &b.code);
    printf("Enter purchase day, month and year: \n ");
    scanf("%d %d %d", &b.date.dd, &b.date.mm, &b.date.yy);
    printf("Book name: %s\n", b.name);
    printf("Author: %s\n", b.author);
    printf("Code: %d\n", b.code);
    printf("Date of purchase: %d/%d/%d\n", b.date.dd, b.date.mm, b.date.yy);

    return 0;
}
