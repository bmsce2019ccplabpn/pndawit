#include <stdio.h>

int main()
{
    int a,b;
    printf("Eter numbers:");
    scanf("%d%d",&a,&b);
    printf("\na=%d\nb=%d",a,b);
    swap(&a,&b);
    printf("\nAfter swapping \na=%d\nb=%d",a,b);
    return 0;
}
void swap(int *p,int *q)
{
    int t;
    t=*q;
    *q=*p;
    *p=t;
}
