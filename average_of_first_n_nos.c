#include <stdio.h>

int main()
{  int i, sum,n;
   
     printf("Enter n\n");
     scanf("%d", &n);
     sum=0;
      for(i=1;i<=n;i++)
        sum+=i;
    
     printf("The average of first 'n' natural numbers is  %.2f \n", (float)sum/n);  
	return 0;
}
