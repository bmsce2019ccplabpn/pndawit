#include <stdio.h>
#include <stdlib.h>

int main()
{
    struct employee
    {
        char name[20];
        int age;
        float slry;
        int doj;
    }e1;
    printf("Enter the name of the employee");
    scanf("%s", e1.name);
    printf("Enter the age");
    scanf("%d", &e1.age);
    printf("Enter the salary");
    scanf("%f", &e1.slry);
    printf("Enter the date of joining (ddmmyyyy)");
    scanf("%d",&e1.doj);
    printf("\nName; %s \n", e1.name);
    printf("Age; %d \n", e1.age);
    printf("Salary; %f \n", e1.slry);
    printf("DOJ; %d \n", e1.doj);
    return 0;
}
