#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{   int i, n, sum=0;
    printf("Enter n; ");
    scanf("%d", &n);
    for(i=2;i<=n; i+=2)
        sum+=pow(i, 2);
    printf("The sum of the first n even numbers is %d", sum);

    return 0;
}
