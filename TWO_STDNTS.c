#include <stdio.h>
#include <stdlib.h>

int main()
{
    struct stud
    {
        char name[20];
        char dept[20];
        float mark;
    }s1, s2, w;
    printf("Enter the name of students: \n ");
    scanf("%s %s", s1.name, s2.name);
    printf("Enter theit department: \n");
    scanf("%s %s", s1.dept, s2.dept);
    printf("Enter their marks; \n");
    scanf("%f %f", &s1.mark, &s2.mark);
    if(s1.mark>s2.mark)
        w=s1;
    else
        w=s2;
    printf("%s from department %s has the gretest mark of %.2f", w.name,
          w.dept, w.mark);
    return 0;
}
