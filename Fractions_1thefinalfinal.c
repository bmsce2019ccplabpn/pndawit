#include <stdio.h>
struct fraction input();
struct fraction add(struct fraction f1,struct fraction f2);
void display(int, int);
struct fraction
{   int a, b;
};
int main()
{     
     struct fraction f1,f2;
    printf("Enter numerator and denominator of fraction 1: ");
	f1=input();
	printf("Enter numerator and denominator of fraction 2: ");
	f2=input();
	struct fraction sum=add(f1,f2);
	display(sum.a,sum.b);
	return 0;
}
 struct fraction input()
 {  fraction f;
     scanf("%d %d", &f.a, &f.b);
     return f;
 } 
 struct fraction add(fraction f1,fraction f2)
 {   int i;
     struct fraction s;
	 s.a=((f1.a*f2.b)+(f2.a*f1.b));
	s.b=(f2.b*f1.b);
  for(i=2;i<s.a/2;i++)
	{ while(s.a%i==0 && s.b%i==0)
		{ s.a/=i;
		  s.b/=i; }
	}
	return s;
 }
 void display(int a, int b)
 {   printf("sum: %d/%d \n", a,b);}