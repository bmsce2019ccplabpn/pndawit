#include <stdio.h>

int main()
{   int a,b,c,d,n,dd,i;
    printf("Enter numerator and denominator of fraction 1: ");
	scanf("%d %d", &a, &b);
	printf("Enter numerator and denominator of fraction 2: ");
	scanf("%d %d", &c, &d);
	n=((a*d)+(c*b));
	dd=(b*d);
  for(i=2;i<n/2;i++)
	{ while(n%i==0 && dd%i==0)
		{ dd/=i;
		 n/=i; }
	}
    printf("sum: %d/%d", n, dd);
	return 0;
}
