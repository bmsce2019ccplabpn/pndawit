#include <stdio.h>
#include <stdlib.h>

int main()
{   int n;
    printf("How many elements?\n");
    scanf("%d", &n);
     int i, sum, arr[n];
        for(i=0;i<n;i++)
        {
            printf("Enter element %d:", i);
            scanf("%d", &arr[i]);
        }
         sum=0;
        for(i=0;i<n;i++)
        {
           sum+=arr[i] ;
        }
        printf("The average of the numbers is %.2f", (float)sum/n);


    return 0;
}
